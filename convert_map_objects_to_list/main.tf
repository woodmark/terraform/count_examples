terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "map" {
    type = map(object({
      name = string
      sid = string  
    }))
    default = {
      "test1" = {
        name = "value1",
        sid = "test1"
      }, 
      "test2" = {
        name = "value2",
        sid = "test1"
      }
    }
}

locals {
  list = [ for value in var.map : "${value}" ]
}

# locals {
#   list = flatten([
#     for value in var.map : [
#       "${value}"
#     ]
#   ])
# }

resource "aws_iam_role" "foobar" {
    count = length(local.list)
    name  = local.list[count.index].name

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "${local.list[count.index].sid}"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}