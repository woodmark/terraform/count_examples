terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "deep_map" {
    type = map(map(list(string)))
    default = {
      "member1" = {
        "dataset1" : ["role1","role2", "role5"],
        "dataset2" : ["role3","role2"],
      },
      "member2" = {
        "dataset3" : ["role1","role4"],
        "dataset2" : ["role5"],
      } 
    }
}

locals {
  # Loop over both lists and flatten the result
  helper_list = flatten([for member, value in var.deep_map:
                 flatten([for dataset, roles in value: 
                           [for role in roles:
                            {"member" = member
                            "dataset" = dataset
                            "role" = role}
                         ]])
                   ])
}

resource "aws_iam_role" "foobar" {
    count = length(local.helper_list)
    name  = "${local.helper_list[count.index].role}-${local.helper_list[count.index].dataset}"

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "${upper(local.helper_list[count.index].member)}"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}