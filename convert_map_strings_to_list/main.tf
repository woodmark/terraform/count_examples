terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.49.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "map" {
    type = map(string)
    default = {"test1" = "value1", "test2" = "value2"}
}

locals {
  list = [ for value in var.map : "${value}" ]
}

# locals {
#   list = flatten([
#     for value in var.map : [
#       "${value}"
#     ]
#   ])
# }

resource "aws_iam_role" "foobar" {
    count = length(local.list)
    name  = local.list[count.index]

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}